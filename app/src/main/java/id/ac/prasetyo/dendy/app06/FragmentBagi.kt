package id.ac.prasetyo.dendy.app06

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_pembagian.*
import kotlinx.android.synthetic.main.frag_pembagian.view.*
import java.text.DecimalFormat

class FragmentBagi : Fragment(), View.OnClickListener {

    var x : Double = 0.0
    var y : Double = 0.0
    var hasil : Double = 0.0

    lateinit var thisParent: MainActivity
    lateinit var v : View

    override fun onClick(v: View?) {
        x = edXb.text.toString().toDouble()
        y = edYb.text.toString().toDouble()
        hasil=x/y
        txHasil.text=DecimalFormat("#.##").format(hasil)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent=activity as MainActivity
        v = inflater.inflate(R.layout.frag_pembagian,container,false)

        v.btnBagi.setOnClickListener(this)
        return v
    }
}